const mongoose = require('mongoose')
const Cliente = mongoose.model('Cliente')

module.exports = {
    async index(req, res) {
        const { page = 1 } = req.query;
        const registros = await Cliente.paginate({}, { page, limit: 10 });

        return res.json(registros)
    },
    async show(req, res) {
        const registro = await Cliente.findById(req.params.id);

        return res.json(registro)
    },
    async update(req, res) {
        const registro = await Cliente.findByIdAndUpdate(req.params.id, req.body, { new: true });

        return res.json(registro)
    },
    async destroy(req, res) {
        await Cliente.findByIdAndRemove(req.params.id);
        return res.send()
    },
    async store(req, res) {
        const registro = await Cliente.create(req.body)

        // Avisa o front-end
        req.io.emit('novoCliente', registro);

        return res.json(registro)
    }
}