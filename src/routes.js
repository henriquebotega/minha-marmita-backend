const express = require('express')
const routes = express.Router()

const ProdutosController = require('./controllers/ProdutosController')
routes.get('/produtos', ProdutosController.index);
routes.get('/produtos/:id', ProdutosController.show);
routes.put('/produtos/:id', ProdutosController.update);
routes.delete('/produtos/:id', ProdutosController.destroy);
routes.post('/produtos', ProdutosController.store);

const ClientesController = require('./controllers/ClientesController')
routes.get('/clientes', ClientesController.index);
routes.get('/clientes/:id', ClientesController.show);
routes.put('/clientes/:id', ClientesController.update);
routes.delete('/clientes/:id', ClientesController.destroy);
routes.post('/clientes', ClientesController.store);

module.exports = routes